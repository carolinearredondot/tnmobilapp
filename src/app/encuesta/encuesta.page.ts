import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.page.html',
  styleUrls: ['./encuesta.page.scss'],
})
export class EncuestaPage implements OnInit {

  firstLoad = true;
  public name: string;
  public lastname: string;
  public email: string;
  public rut: string;
  public id: string;
  public pacienteId: string;
  public controles: any[];
  public token: string;
  public rutinaActualId: string;
  public controlActual: any;
  public rutinaNombre: string;
  ionicForm: FormGroup;
  isSubmitted = false;

  public ejercicios: any[];
  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient
  ) {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    this.id = user['id'];
    this.name = user['name'];
    this.lastname = user['lastname'];
    this.email = user['email'];
    this.rut = user['rut'];
    this.pacienteId = user.patient['id'];
    console.log(this.id)
    console.log(this.pacienteId)
    this.token = localStorage.getItem('token')
    this.rutinaActualId = localStorage.getItem('rutinaActualId')
   }

  ngOnInit() {
    this.obtenerRutina(this.token,this.rutinaActualId)
    this.ionicForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})")]],
      password: ['', [Validators.required, Validators.minLength(2)]],
    })
  }
  get errorControl() {
    return this.ionicForm.controls;
  }

  submitForm(form: NgForm) {
    console.log(form)
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Por favor ingrese los datos solicitados')
      return false;
    }
    else {
      let data = {
        email: form.value.email,
        password: form.value.password
      };
      this.http.post('http://localhost:3005/login', data).subscribe(async response => {
        console.log(response)
        // await this.obtenerUser(response['access_token'])
        // this.iniciarSesion();
      },
        error => {
          console.log(error)
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Credenciales invalidas',
          })
        }
      )
    }
  }

  async obtenerRutina(token,rutinaId){
    const headers = new HttpHeaders().set('Authorization','bearer '+token);
    try{
      const response = await this.http.get<Array<any>>('http://localhost:3005/routine/'+rutinaId, {
        headers
      }).toPromise();
      console.log(response)
      this.controlActual = response;
    }
    catch(error){
      console.log(error)
    }
  }
}
