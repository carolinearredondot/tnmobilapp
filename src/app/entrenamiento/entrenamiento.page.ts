import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-entrenamiento',
  templateUrl: './entrenamiento.page.html',
  styleUrls: ['./entrenamiento.page.scss'],
})
export class EntrenamientoPage implements OnInit {
    public name: string;
    public lastname: string;
    public email: string;
    public rut: string;
    public id: string;
    public pacienteId: string;
    public controles: any[];
    public token: string;

    constructor(
    private router: Router,
    private http: HttpClient
  ) {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    this.id = user['id'];
    this.name = user['name'];
    this.lastname = user['lastname'];
    this.email = user['email'];
    this.rut = user['rut'];
    this.pacienteId = user.patient['id'];
    console.log(this.id)
    console.log(this.pacienteId)
    this.token = localStorage.getItem('token')
  }

  back(){
    this.router.navigate(['/perfil']);
   } 

   verControl(id){
      localStorage.setItem('controlActualId',id);
     this.router.navigate(['/ver-control']);
   }

  ngOnInit() {
    this.obtenerControl(this.token)
  }
  async obtenerControl(token){
    const headers = new HttpHeaders().set('Authorization','bearer '+token);
    try{
      const response = await this.http.get<Array<any>>('http://localhost:3005/control/'+this.pacienteId+ '/patient', {
        headers
      }).toPromise();
      console.log(response)
      this.controles = response;
    }
    catch(error){
      console.log(error)
    }
  }

}
