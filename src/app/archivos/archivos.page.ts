import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.page.html',
  styleUrls: ['./archivos.page.scss'],
})
export class ArchivosPage implements OnInit {

  public name: string;
  public lastname: string;
  public email: string;
  public rut: string;
  public id: string;
  public files: any[];
  constructor(

    private router: Router,
    private http: HttpClient,

  ) 
  {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    this.id = user['id'];
    this.name = user['name'];
    this.lastname = user['lastname'];
    this.email = user['email'];
    this.rut = user['rut'];
    console.log(this.id)
  }
  back(){
    this.router.navigate(['/perfil']);
   } 
  ngOnInit() {
    this.obtenerArchivos(this.id)
  }

   
  async descargarArchivo(id,filename){
    const url = 'http://localhost:3005/files/'+id + '/download'
    window.open(url, '_blank')
  }


  async obtenerArchivos(token){
    const headers = new HttpHeaders().set('Authorization','bearer '+token);
    try{
      const response = await this.http.get<Array<any>>('http://localhost:3005/files/', {
        headers
      }).toPromise();
      console.log(response)
      this.files = response
    }
    catch(error){
      console.log(error)
    }
  }
}
