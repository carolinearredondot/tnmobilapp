import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.page.html',
  styleUrls: ['./password-recovery.page.scss'],
})
export class PasswordRecoveryPage implements OnInit {

  ionicForm: FormGroup;
  public email: string;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private toastController: ToastController,
  ) {

  }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})")]],
    })
  }

  async submitEmail(form: NgForm){
    if (!this.ionicForm.valid) {
      console.log('Por favor ingrese los datos solicitados')
      return false;
    }
    this.email = form.value['email'];
    let toast: HTMLIonToastElement;
    try{
      const url = `http://localhost:3005/users/${this.email}/forgot`;
      const response = await this.http.patch(url,{}).toPromise();
      toast = await this.toastController.create({
        header: 'Correo enviado',
        icon: 'checkmark-done-circle-outline',
        duration: 3000
      });
      await toast.present();
      this.router.navigate(['/home']);
    }catch(e){
      toast = await this.toastController.create({
        header: 'Usuario no encontrado o error en el sistema',
        icon: 'alert-circle-outline',
        duration: 5000
      });
      await toast.present();
    }

  }
}
