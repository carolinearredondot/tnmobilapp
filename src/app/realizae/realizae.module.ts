import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RealizaePageRoutingModule } from './realizae-routing.module';

import { RealizaePage } from './realizae.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RealizaePageRoutingModule
  ],
  declarations: [RealizaePage]
})
export class RealizaePageModule {}
