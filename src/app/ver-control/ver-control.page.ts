import { Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ver-control',
  templateUrl: './ver-control.page.html',
  styleUrls: ['./ver-control.page.scss'],
})
export class VerControlPage implements OnInit {

  public name: string;
  public lastname: string;
  public email: string;
  public rut: string;
  public id: string;
  public pacienteId: string;
  public controlAttachments: any[];
  public control: any;
  public token: string;
  public controlActualId : string;
  public waist: string;
  public height: string;
  public weight: string;
  public media_url: string;

  constructor(
  private router: Router,
  private http: HttpClient
) {
  const user = JSON.parse(localStorage.getItem('user'))
  console.log(user)
  this.id = user['id'];
  this.name = user['name'];
  this.lastname = user['lastname'];
  this.email = user['email'];
  this.rut = user['rut'];
  this.pacienteId = user.patient['id'];
  console.log(this.id)
  console.log(this.pacienteId)
  this.token = localStorage.getItem('token')
  this.controlActualId = localStorage.getItem('controlActualId')
}

back(){
  this.router.navigate(['/entrenamiento']);
 } 

 verRutina(id){
    localStorage.setItem('rutinaActualId',id);
   this.router.navigate(['/ver-rutina']);
 }

async ngOnInit() {
  await this.obtenerAtachment(this.token,this.controlActualId),
  await this.obtenerControl(this.token,this.controlActualId)
}
async obtenerAtachment(token,controlId){
  const headers = new HttpHeaders().set('Authorization','bearer '+token);
  try{
    const response = await this.http.get<Array<any>>('http://localhost:3005/control_attachment/'+controlId+ '/control', {
      headers
    }).toPromise();
    console.log(response)
    this.controlAttachments = response;
  }
  catch(error){
    console.log(error)
  }
}

async descargarArchivo(id,filename){
  let config = {
    responseType: "blob",
    
    method:'get',
    url: "http://localhost:3005/files/"+id +"/download",
  }
  try{
    console.log(id);
    const response = await this.http.get("http://localhost:3005/files/"+id +"/download",{ responseType: "blob"}).toPromise();
    const url = window.URL.createObjectURL(new Blob([response]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', filename); //or any other extension
    document.body.appendChild(link);
    link.click();
  }catch{

  }
  // const url = 'http://localhost:3005/files/'+id + '/download'
  // window.open(url, '_blank')
}


async obtenerControl(token,idControl){
  const headers = new HttpHeaders().set('Authorization','bearer '+token);
  try{
    const response = await this.http.get<Array<any>>('http://localhost:3005/control/'+idControl, {
      headers
    }).toPromise();
    console.log('control',response)
    this.control = response;
    this.height = response['height'];
    this.weight = response['weight'];
    this.waist = response['waist'];
    this.media_url = response['media_url'];
  }
  catch(error){
    console.log(error)
  }
}

}
