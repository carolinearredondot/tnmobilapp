import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerControlPage } from './ver-control.page';

const routes: Routes = [
  {
    path: '',
    component: VerControlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerControlPageRoutingModule {}
