import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  public name: string;
  public lastname: string;
  public email: string;
  public rut: string;

  constructor(private router: Router, private http: HttpClient) {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    this.name = user['name'];
    this.lastname = user['lastname'];
    this.email = user['email'];
    this.rut = user['rut'];
  }

  public options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: 'Progreso disminucion de peso',
      },
    },
  };
  public labels = [
    'Control 1',
    'Control 2',
    'Control 3',
    'Control 4 ',
    'Control 5',
    'Control 6',
    'Control 7',
  ];
  public data;

  async ngOnInit() {
    await this.obtenerControles()
  }

  goArchivos() {
    this.router.navigate(['/archivos']);
  }

  goEntrenamiento() {
    this.router.navigate(['/entrenamiento']);
  }

  salir() {
    this.router.navigate(['/home']);
  }

  async obtenerControles() {
    const token = localStorage.getItem('token');
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user);
    const headers = new HttpHeaders().set('Authorization', 'bearer ' + token);
    try {
      const response = await this.http
        .get<Array<any>>('http://localhost:3005/control/'+user.patient.id+'/patient', {
          headers,
        })
        .toPromise();
      let pesos = response.map((u) => {
        return u.weight;
      });
      pesos = pesos.slice(0, 7);
      pesos.reverse();
      this.data = {
        labels: this.labels,
        datasets: [
          {
            label: 'Pesos del paciente',
            data: pesos,
            borderColor: 'rgb(255, 99, 132)',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
          },
        ],
      };
      console.log(this.data)
    } catch (error) {
      console.log(error);
    }
  }
  // async obtenerUser(token){
  //   const headers = new HttpHeaders().set('Authorization','bearer '+token);
  //   try{
  //     const response = await this.http.get('http://localhost:3005/user/token', {
  //       headers
  //     }).toPromise();
  //     console.log(response)
  //   }
  //   catch(error){
  //     console.log(error)
  //   }
  // }
}
