import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from "rxjs/operators";
import axios from "axios";
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { environment, SECRET_KEY } from 'environments/environment';



@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public name: string;
  public lastname: string;
  public email: string;


  ionicForm: FormGroup;
  isSubmitted = false;


  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient
  ) {
  }

  iniciarSesion() {
    this.router.navigate(['/perfil']);
  }

  goToPR() {
    this.router.navigate(['/password-recovery']);
  }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})")]],
      password: ['', [Validators.required, Validators.minLength(2)]],
    })
  }

  get errorControl() {
    return this.ionicForm.controls;
  }


  submitForm(form: NgForm) {
    console.log(form)
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Por favor ingrese los datos solicitados')
      return false;
    }
    else {
      let data = {
        email: form.value.email,
        password: form.value.password
      };
      this.http.post('http://localhost:3005/login', data).subscribe(async response => {
        console.log(response)
        await this.obtenerUser(response['access_token'])
        this.iniciarSesion();
      },
        error => {
          console.log(error)
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Credenciales invalidas',
          })
        }
      )
    }
  }

  async obtenerUser(token){
    const headers = new HttpHeaders().set('Authorization','bearer '+token);
    try{
      const response = await this.http.get('http://localhost:3005/users/token', {
        headers
      }).toPromise();
      console.log(response)
      localStorage.setItem('token',token);
      localStorage.setItem('user',JSON.stringify(response))
      this.name = response['name'];
      this.lastname = response['lastname'];
      
    }
    catch(error){
      console.log(error)
    }
  }



}
