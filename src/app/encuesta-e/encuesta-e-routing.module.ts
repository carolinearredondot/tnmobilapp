import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EncuestaEPage } from './encuesta-e.page';

const routes: Routes = [
  {
    path: '',
    component: EncuestaEPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EncuestaEPageRoutingModule {}
