import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-realizae',
  templateUrl: './realizae.page.html',
  styleUrls: ['./realizae.page.scss'],
})
export class RealizaePage implements OnInit {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    
   }

  back(){
    this.router.navigate(['/entrenamiento']);
   } 


  ngOnInit() {
  }

}
