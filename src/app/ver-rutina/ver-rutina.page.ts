import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';


@Component({
  selector: 'app-ver-rutina',
  templateUrl: './ver-rutina.page.html',
  styleUrls: ['./ver-rutina.page.scss']
})
export class VerRutinaPage implements OnInit {
  @ViewChild('slider') private slider: IonSlides;
  firstLoad = true;
  public name: string;
  public lastname: string;
  public email: string;
  public rut: string;
  public id: string;
  public pacienteId: string;
  public controles: any[];
  public token: string;
  public rutinaActualId: string;
  public rutinaActual: any;
  public rutinaNombre: string;

  public ejercicios: any[];

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    this.id = user['id'];
    this.name = user['name'];
    this.lastname = user['lastname'];
    this.email = user['email'];
    this.rut = user['rut'];
    this.pacienteId = user.patient['id'];
    console.log(this.id)
    console.log(this.pacienteId)
    this.token = localStorage.getItem('token')
    this.rutinaActualId = localStorage.getItem('rutinaActualId')
    
   }
   async loadPrev() {
    console.log('Prev');
    let newIndex = await this.slider.getActiveIndex();
    newIndex++;
    // this.ejercicios.unshift(this.ejercicios[0] - 1);
    // this.ejercicios.pop();
    this.slider.slidePrev();

    console.log(`New status: ${this.ejercicios}`);
    }

    async loadNext() {
        if(this.firstLoad) {
          // Since the initial slide is 1, prevent the first 
          // movement to modify the slides
          this.firstLoad = false;
          return;
        }

        console.log('Next');
        let newIndex = await this.slider.getActiveIndex();

        newIndex--;
        // this.ejercicios.push(this.ejercicios[this.ejercicios.length - 1] + 1);
        // this.ejercicios.shift();

        // Workaround to make it work: breaks the animation
        this.slider.slideNext();
    }

   back(){
    this.router.navigate(['/ver-control']);
   } 

  ngOnInit() {
    this.obtenerRutina(this.token,this.rutinaActualId)
  }

  async obtenerRutina(token,rutinaId){
    const headers = new HttpHeaders().set('Authorization','bearer '+token);
    try{
      const response = await this.http.get<Array<any>>('http://localhost:3005/routine/'+rutinaId, {
        headers
      }).toPromise();
      console.log(response)
      this.rutinaActual = response;
      this.rutinaNombre = response['order'];
      this.ejercicios = response['exercises'];
    }
    catch(error){
      console.log(error)
    }
  }

 

}
