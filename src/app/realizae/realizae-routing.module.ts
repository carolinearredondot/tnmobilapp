import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RealizaePage } from './realizae.page';

const routes: Routes = [
  {
    path: '',
    component: RealizaePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RealizaePageRoutingModule {}
