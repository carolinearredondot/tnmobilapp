import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Perfil', url: '/perfil', icon: 'person' },
    { title: 'Archivos', url: '/archivos', icon: 'document' },
    { title: 'Entrenamiento', url: '/entrenamiento', icon: 'barbell' },
    { title: 'Cerrar Sesión', url: '/cerrar', icon: 'log-out' }
  ];
  constructor() {}
}
