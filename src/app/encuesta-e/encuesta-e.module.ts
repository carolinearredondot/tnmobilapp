import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EncuestaEPageRoutingModule } from './encuesta-e-routing.module';

import { EncuestaEPage } from './encuesta-e.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EncuestaEPageRoutingModule
  ],
  declarations: [EncuestaEPage]
})
export class EncuestaEPageModule {}
