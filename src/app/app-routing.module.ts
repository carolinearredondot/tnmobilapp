import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'archivos',
    loadChildren: () => import('./archivos/archivos.module').then( m => m.ArchivosPageModule)
  },
  {
    path: 'cerrar',
    loadChildren: () => import('./cerrar/cerrar.module').then( m => m.CerrarPageModule)
  },
  {
    path: 'password-recovery',
    loadChildren: () => import('./password-recovery/password-recovery.module').then( m => m.PasswordRecoveryPageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'entrenamiento',
    loadChildren: () => import('./entrenamiento/entrenamiento.module').then( m => m.EntrenamientoPageModule)
  },
  {
    path: 'realizae',
    loadChildren: () => import('./realizae/realizae.module').then( m => m.RealizaePageModule)
  },  {
    path: 'ver-control',
    loadChildren: () => import('./ver-control/ver-control.module').then( m => m.VerControlPageModule)
  },
  {
    path: 'ver-rutina',
    loadChildren: () => import('./ver-rutina/ver-rutina.module').then( m => m.VerRutinaPageModule)
  },
  {
    path: 'encuesta',
    loadChildren: () => import('./encuesta/encuesta.module').then( m => m.EncuestaPageModule)
  },
  {
    path: 'encuesta-e',
    loadChildren: () => import('./encuesta-e/encuesta-e.module').then( m => m.EncuestaEPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
