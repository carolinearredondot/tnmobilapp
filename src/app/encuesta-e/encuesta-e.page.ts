import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-encuesta-e',
  templateUrl: './encuesta-e.page.html',
  styleUrls: ['./encuesta-e.page.scss'],
})
export class EncuestaEPage implements OnInit {

  firstLoad = true;
  public name: string;
  public lastname: string;
  public email: string;
  public rut: string;
  public id: string;
  public pacienteId: string;
  public controles: any[];
  public token: string;
  public rutinaActualId: string;
  public rutinaActual: any;
  public rutinaNombre: string;

  public ejercicios: any[];
  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    this.id = user['id'];
    this.name = user['name'];
    this.lastname = user['lastname'];
    this.email = user['email'];
    this.rut = user['rut'];
    this.pacienteId = user.patient['id'];
    console.log(this.id)
    console.log(this.pacienteId)
    this.token = localStorage.getItem('token')
    this.rutinaActualId = localStorage.getItem('rutinaActualId')
  }

  ngOnInit() {
    this.obtenerRutina(this.token, this.rutinaActualId)
  }
  async obtenerRutina(token, rutinaId) {
    const headers = new HttpHeaders().set('Authorization', 'bearer ' + token);
    try {
      const response = await this.http.get<Array<any>>('http://localhost:3005/routine/' + rutinaId, {
        headers
      }).toPromise();
      console.log(response)
      this.rutinaActual = response;
      this.rutinaNombre = response['order'];
      this.ejercicios = response['exercises'];
    }
    catch (error) {
      console.log(error)
    }
  }
}
