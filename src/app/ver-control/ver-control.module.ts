import { SafePipe } from './../safe.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerControlPageRoutingModule } from './ver-control-routing.module';

import { VerControlPage } from './ver-control.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerControlPageRoutingModule
  ],
  declarations: [VerControlPage, SafePipe]
})
export class VerControlPageModule {}
